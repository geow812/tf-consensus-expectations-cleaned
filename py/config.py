import os
import logging

# define file path
ROOT_DIR = os.path.abspath('.')
# 统一定义，方便未来可能的改名
INPUT_DIR = '数据'
OUTPUT_DIR = 'output'
DEMO_CSV_PATH = os.path.join(ROOT_DIR, INPUT_DIR, 'demo.csv')
OUTPUT_CSV_PATH = os.path.join(ROOT_DIR, OUTPUT_DIR, 'result.csv')

# define constants
DEMO_NUMBER = 888
DEMO_STR = "This is a demo message"

# data_NETPROFIT_PARENTNETPROFIT_root = '/Users/chenxj00/Documents/长江证券/改善一致预期/数据/净利润-归母净利润.csv'
# data_KCFJCXSYJLR_root = '/Users/chenxj00/Documents/长江证券/改善一致预期/数据/扣除非经常损益后的净利润.csv'
# analyst_prediction_root = '/Users/chenxj00/Documents/长江证券/改善一致预期/数据/一致性预期数据-净利润.csv'
data_NETPROFIT_PARENTNETPROFIT_root = os.path.join(ROOT_DIR, INPUT_DIR, '净利润-归母净利润.csv')
data_KCFJCXSYJLR_root = os.path.join(ROOT_DIR, INPUT_DIR, '扣除非经常损益后的净利润.csv')
analyst_prediction_root = os.path.join(ROOT_DIR, INPUT_DIR, '一致性预期数据-净利润.csv')

OUTPUT_ROOT = 'final_re_try1.csv'
# define log format
LOG_FILE_NAME = os.path.join(ROOT_DIR, 'log', 'demo_project.log')
LOG_FORMAT = '%(asctime)s %(levelname)s %(funcName)s(%(lineno)d): %(message)s'
loggingConfig = {
    'level': logging.INFO,
    'format': LOG_FORMAT,
    'datefmt': '%Y-%m-%d %H:%M:%S',
    'filename': LOG_FILE_NAME,
    'filemode': 'a'
}

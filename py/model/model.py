# All models here: import statsmodel, scikit-learn etc.
import pandas as pd
import logging
from config import LOG_FILE_NAME

import datetime
import re
import time
from data_process.data_process import time_judge
logger = logging.getLogger(LOG_FILE_NAME)


def prediction(total):
    total['predicttype'] = total['managesituationGreatChange']
    total['predict'] = total['managesituationGreatChange']
    for i in range(total.shape[0]):
        try:
            #DP_1 = total['KCFJCXSYJLR'][i - 4]
            DP_1=total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-1)) & (total['Month'] == total['Month'][i])].tolist()[0]
            #DP_2 = total['KCFJCXSYJLR'][i - 8]
            DP_2=total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-2)) & (total['Month'] == total['Month'][i])].tolist()[0]

            #DP_3 = total['KCFJCXSYJLR'][i - 12]
            DP_3=total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-3)) & (total['Month'] == total['Month'][i])].tolist()[0]
            DP_1_4 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-1)) & (total['Month'] == 12)].tolist()[0]
            DP_2_4 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i] - 2)) & (total['Month'] == 12)].tolist()[0]
            DP_3_4 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i] - 3)) & (total['Month'] == 12)].tolist()[0]
            DP4_1 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i] - 1)) & (total['Month'] == 12)].tolist()[0]
            DPn_1 = total['KCFJCXSYJLR'][
                (total['Year'] == (total['Year'][i]-1)) & (total['Month'] == total['Month'][i])].tolist()[0]
            DPn = total['KCFJCXSYJLR'][i]

            DP4_2 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-2)) & (total['Month'] == 12)].tolist()[0]
            DP4_3 = total['KCFJCXSYJLR'][(total['Year'] == (total['Year'][i]-3)) & (total['Month'] == 12)].tolist()[0]
            DPn_2 = total['KCFJCXSYJLR'][
                (total['Year'] == (total['Year'][i]-2)) & (total['Month'] == total['Month'][i])].tolist()[0]
            DPn_3 = total['KCFJCXSYJLR'][
                (total['Year'] == (total['Year'][i]-3)) & (total['Month'] == total['Month'][i])].tolist()[0]
            pnt = total['PARENTNETPROFIT_x'][i]
            p4_1 =total['PARENTNETPROFIT_x'][(total['Year'] == (total['Year'][i]-1)) & (total['Month'] == 12)].tolist()[0]
            n = total['Month'][i]

            if (total['managesituationGreatChange'][i] == True) & (total['IfprofitdisStable'][i] == True):
                total['predicttype'][i] = '比例外推'
                PCT1 = DP_1 / DP_1_4
                PCT2 = DP_2 / DP_2_4
                PCT3 = DP_3 / DP_3_4
                PCT_mean = (PCT1 + PCT2 + PCT3) / 3
                DP = total['KCFJCXSYJLR'][i]
                predict = (DP / PCT_mean - DP) + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == True) & (total['IfprofitdisStable'][i] == False) & (
                    total['ifhistoryprofitextroplate'][i] == True):
                total['predicttype'][i] = '平均增速'
                AVGgyoy = ((DP4_1 - DP4_2) / (abs(DP4_2)) + (DP4_2 - DP4_3) / (abs(DP4_3))) / 2
                gnt = (DPn - DPn_1) / abs(DPn_1)
                AVGgnt = (AVGgyoy + gnt) / 2
                predict = (DP4_1 - DPn_1) * (1 + AVGgnt) + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == True) & (total['IfprofitdisStable'][i] == False) & (
                    total['ifhistoryprofitextroplate'][i] == False):
                total['predicttype'][i] = '前期平均'
                Pnt = total['PARENTNETPROFIT_x'][i].tolist()[0]
                predict = (DP4_2 - DPn_2 + DP4_3 - DPn_3) / 2 + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0))) & (
                    total['ifhistoryprofitextroplate'][i] == True):
                total['predicttype'][i] = '前期平均'
                predict = (DP4_2 - DPn_2 + DP4_3 - DPn_3) / 2 + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0))) & (
                    total['ifhistoryprofitextroplate'][i] == False):
                total['predicttype'][i] = '去年同期'
                predict = DP4_1 - DPn_1 + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0))) & (pnt > 0) & (
                    (total['PARENTNETPROFIT_x'][i - 8] > 0) & (total['PARENTNETPROFIT_x'][i - 12] > 0)):
                total['predicttype'][i] = '前期平均'
                predict = (DP4_2 - DPn_2 + DP4_3 - DPn_3) / 2 + pnt
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0))) & (pnt > 0) & (
                    (total['PARENTNETPROFIT_x'][i - 8] < 0) | (total['PARENTNETPROFIT_x'][i - 12] < 0)):
                total['predicttype'][i] = '盈利变化'
                predict = p4_1 + 4 * (DPn - DPn_1) / n
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0))) & (pnt < 0) & (
                    total['ifhistoryprofitextroplate'][i] == True):
                total['predicttype'][i] = '盈利变化'
                predict = p4_1 + 4 * (DPn - DPn_1) / n
                total['predict'][i] = predict
            elif (total['managesituationGreatChange'][i] == False) & (
                    (((total['PARENTNETPROFIT_x'][i]) > 0) & ((total['PARENTNETPROFIT_x'][i - 4]) < 0)) | (
                    ((total['PARENTNETPROFIT_x'][i]) < 0) & ((total['PARENTNETPROFIT_x'][i - 4]) > 0))) & (pnt < 0) & (
                    total['ifhistoryprofitextroplate'][i] == False):
                total['predicttype'][i] = '前期平均盈利变化'
                predict = (DP4_2 - DPn_2 + DP4_3 - DPn_3) / 2 + (4 - n) * (DPn - DPn_1) + pnt
                total['predict'][i] = predict
        except:
            total['predicttype'][i] = '未知'
            total['predict'][i] = '未知'
    return total


def adj_ana_prediction(x, total, analyst_prediction):
    data_60025_analyst_prediction = analyst_prediction
    data_60025_analyst_prediction['code'] = data_60025_analyst_prediction['S_INFO_WINDCODE'].map(
        lambda x: ''.join(re.findall('[\d]', x)))
    data_60025_analyst_prediction = data_60025_analyst_prediction[data_60025_analyst_prediction['code'] == x]

    data_60025_analyst_prediction['EST_REPORT_DT'] = pd.to_datetime(data_60025_analyst_prediction['EST_REPORT_DT'],
                                                                    format='%Y%m%d')
    data_60025_analyst_prediction['Year'] = data_60025_analyst_prediction['EST_REPORT_DT'].map(lambda x: x.year)
    data_60025_analyst_prediction['Month'] = data_60025_analyst_prediction['EST_REPORT_DT'].map(lambda x: x.month)
    data_60025_analyst_prediction['EST_DT'] = pd.to_datetime(data_60025_analyst_prediction['EST_DT'], format='%Y%m%d')
    data_60025_analyst_prediction['When_Report_Year'] = data_60025_analyst_prediction['EST_DT'].map(lambda x: x.year)
    data_60025_analyst_prediction['When_Report_Month'] = data_60025_analyst_prediction['EST_DT'].map(lambda x: x.month)

    data_60025_analyst_prediction['When_Report_Month'] = data_60025_analyst_prediction['When_Report_Month'].map(
        time_judge)
    data_60025_analyst_prediction = data_60025_analyst_prediction[
        data_60025_analyst_prediction['Year'] == data_60025_analyst_prediction['When_Report_Year']]
    #data_60025_analyst_prediction_group = data_60025_analyst_prediction.groupby(['When_Report_Year', 'When_Report_Month','CONSEN_DATA_CYCLE_TYP'])[
       #['NET_PROFIT_AVG']].mean().reset_index()
    df_temp = data_60025_analyst_prediction[data_60025_analyst_prediction['CONSEN_DATA_CYCLE_TYP'] == 263002000]
    df_temp=df_temp[df_temp.groupby(['When_Report_Year', 'When_Report_Month'])['EST_DT'].transform(max) == df_temp['EST_DT']].groupby(['When_Report_Year', 'When_Report_Month']).mean().reset_index()
    data_60025_analyst_prediction_group=df_temp.copy()
    total['analyst_prediction'] ='未知'
    for i in range(total.shape[0]):
        for j in range(data_60025_analyst_prediction_group.shape[0]):
            if (total['Year'][i] == data_60025_analyst_prediction_group['When_Report_Year'][j]) & (
                    total['Month'][i] == data_60025_analyst_prediction_group['When_Report_Month'][j])&(data_60025_analyst_prediction_group['CONSEN_DATA_CYCLE_TYP'][j]==263002000):
                total['analyst_prediction'][i] = data_60025_analyst_prediction_group['NET_PROFIT_AVG'][j] * 10000
    total['adj_prediction'] = total['predict']
    for i in range(total.shape[0]):
        try:

            if abs((total['predict'][i] - total['analyst_prediction'][i]) / total['analyst_prediction'][i]) > 0.1:
                total['adj_prediction'][i] = total['analyst_prediction'][i]
            else:
                total['adj_prediction'][i] = total['predict'][i]
        except:
            total['adj_prediction'][i] = total['predict'][i]
    return total
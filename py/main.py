import logging

import pandas as pd

from config import data_NETPROFIT_PARENTNETPROFIT_root, data_KCFJCXSYJLR_root, analyst_prediction_root, OUTPUT_ROOT
from config import loggingConfig, LOG_FILE_NAME
from data_process.data_process import judge
from model.model import prediction, adj_ana_prediction

# configure log file
logging.basicConfig(**loggingConfig)
logger = logging.getLogger(LOG_FILE_NAME)


def demo_func(code):
    # read data from database, 注释写中文也可以
    # read file from input directory
    # input_csv_data = pd.read_csv(DEMO_CSV_PATH)

    analyst_prediction = pd.read_csv(analyst_prediction_root, header=0)
    total1 = judge(x=code, data_NETPROFIT_PARENTNETPROFIT=data_NETPROFIT_PARENTNETPROFIT,
                   data_KCFJCXSYJLR=data_KCFJCXSYJLR)
    total2 = prediction(total=total1)
    total3 = adj_ana_prediction(x=code, total=total2, analyst_prediction=analyst_prediction)
    total3.to_csv('output/'+code+OUTPUT_ROOT)

stock_list_data= pd.read_csv(data_NETPROFIT_PARENTNETPROFIT_root, dtype={'SECURITYCODE': str},
                                                 header=0).dropna(how='any').drop_duplicates(keep='first')
stock_list_data['SECURITYCODE'] = stock_list_data['SECURITYCODE'].map(lambda x: str(x).zfill(6))
stock_list=stock_list_data['SECURITYCODE'].unique().tolist()
data_NETPROFIT_PARENTNETPROFIT = pd.read_csv(data_NETPROFIT_PARENTNETPROFIT_root, dtype={'SECURITYCODE': str},
                                                 header=0).dropna(how='any').drop_duplicates(keep='first')
data_NETPROFIT_PARENTNETPROFIT['SECURITYCODE'] = data_NETPROFIT_PARENTNETPROFIT['SECURITYCODE'].map(lambda x: str(x).zfill(6))

data_NETPROFIT_PARENTNETPROFIT=data_NETPROFIT_PARENTNETPROFIT[data_NETPROFIT_PARENTNETPROFIT.groupby(['SECURITYCODE','REPORTDATE'])['FIRSTNOTICEDATE'].transform(max)==data_NETPROFIT_PARENTNETPROFIT['FIRSTNOTICEDATE']].reset_index().drop(['index'],axis=1)
data_KCFJCXSYJLR = pd.read_csv(data_KCFJCXSYJLR_root, dtype={'SECURITYCODE': str}, header=0).dropna(
        ).drop_duplicates(keep='first')
data_KCFJCXSYJLR['SECURITYCODE'] = data_KCFJCXSYJLR['SECURITYCODE'].map(lambda x: str(x).zfill(6))
data_KCFJCXSYJLR=data_KCFJCXSYJLR[data_KCFJCXSYJLR.groupby(['SECURITYCODE','REPORTDATE'])['NOTICEDATE'].transform(max)==data_KCFJCXSYJLR['NOTICEDATE']].reset_index().drop(['index'],axis=1)
data_NETPROFIT_PARENTNETPROFIT.to_csv(data_NETPROFIT_PARENTNETPROFIT_root)
data_KCFJCXSYJLR.to_csv(data_KCFJCXSYJLR_root)

if __name__ == "__main__":
    # 调试可以写log，可以看到log/demo_project.log文件被写入
    # 也可以print，但最重要的提示信息，如运行时间、运行结果、错误信息需要写log
    logger.info('run demo main')
    for i in range(1000,len(stock_list)):
        demo_func(stock_list[i])
    #demo_func('600507')
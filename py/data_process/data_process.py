import logging
from config import LOG_FILE_NAME
import pandas as pd
import numpy as np
logger = logging.getLogger(LOG_FILE_NAME)
import re


def judge(x, data_NETPROFIT_PARENTNETPROFIT, data_KCFJCXSYJLR):
    data_600529_NETPROFIT_PARENTNETPROFIT = data_NETPROFIT_PARENTNETPROFIT[
        (data_NETPROFIT_PARENTNETPROFIT['SECURITYCODE'] == x)]
    data_600529_NETPROFIT_PARENTNETPROFIT = data_600529_NETPROFIT_PARENTNETPROFIT.drop(
        data_600529_NETPROFIT_PARENTNETPROFIT[
            data_600529_NETPROFIT_PARENTNETPROFIT['REPORTTIMETYPE'] == '7-9月季报'].index)
    data_600529_NETPROFIT_PARENTNETPROFIT['REPORTDATE'] = pd.to_datetime(
        data_600529_NETPROFIT_PARENTNETPROFIT['REPORTDATE'], format='%Y-%m-%d')
    data_600529_NETPROFIT_PARENTNETPROFIT['FIRSTNOTICEDATE'] = pd.to_datetime(
        data_600529_NETPROFIT_PARENTNETPROFIT['FIRSTNOTICEDATE'], format='%Y-%m-%d')

    data_600529_NETPROFIT_PARENTNETPROFIT['Year'] = data_600529_NETPROFIT_PARENTNETPROFIT['REPORTDATE'].map(
        lambda x: x.year)
    data_600529_NETPROFIT_PARENTNETPROFIT['Month'] = data_600529_NETPROFIT_PARENTNETPROFIT['REPORTDATE'].map(
        lambda x: x.month)

    data_600529_NETPROFIT_PARENTNETPROFIT = data_600529_NETPROFIT_PARENTNETPROFIT.reset_index().drop(['index'], axis=1).drop_duplicates(
        keep='first').reset_index().drop(['index','FIRSTNOTICEDATE'], axis=1)

    data_600529_NETPROFIT_PARENTNETPROFIT['managesituationChangeRate'] = data_600529_NETPROFIT_PARENTNETPROFIT[
        'NETPROFIT']
    data_600529_NETPROFIT_PARENTNETPROFIT['managesituationGreatChange'] = data_600529_NETPROFIT_PARENTNETPROFIT[
        'NETPROFIT']

    # 今年已实现净利润是否正常
    for i in range(data_600529_NETPROFIT_PARENTNETPROFIT.shape[0]):
        try:
            year = data_600529_NETPROFIT_PARENTNETPROFIT['Year'][i]
            month = data_600529_NETPROFIT_PARENTNETPROFIT['Month'][i]
            RP_T= data_600529_NETPROFIT_PARENTNETPROFIT['NETPROFIT'].tolist()[0]
            RP_T_1 = data_600529_NETPROFIT_PARENTNETPROFIT[
                (data_600529_NETPROFIT_PARENTNETPROFIT['Year'] == (year - 1)) & (
                        data_600529_NETPROFIT_PARENTNETPROFIT['Month'] == month)]['NETPROFIT'].tolist()[0]
        #data_600529_NETPROFIT_PARENTNETPROFIT['managesituationChangeRate'][i] = (abs(
                #data_600529_NETPROFIT_PARENTNETPROFIT['NETPROFIT'][i] -
                #data_600529_NETPROFIT_PARENTNETPROFIT['NETPROFIT'][i - 4])) / (abs(
                #data_600529_NETPROFIT_PARENTNETPROFIT['NETPROFIT'][i - 4]))
            data_600529_NETPROFIT_PARENTNETPROFIT['managesituationChangeRate'][i]=(RP_T-RP_T_1)/RP_T_1
            if (RP_T > 0) & (
                    RP_T_1 > 0) & (
                    data_600529_NETPROFIT_PARENTNETPROFIT['managesituationChangeRate'][i] < 0.5):
                data_600529_NETPROFIT_PARENTNETPROFIT['managesituationGreatChange'][i] = True
            else:
                data_600529_NETPROFIT_PARENTNETPROFIT['managesituationGreatChange'][i] = False
        except:
            data_600529_NETPROFIT_PARENTNETPROFIT['managesituationChangeRate'][i] = '未知'
            data_600529_NETPROFIT_PARENTNETPROFIT['managesituationGreatChange'][i] = '未知'

    data_PARENTNETPROFIT_KCFJCXSYJLR = pd.merge(data_NETPROFIT_PARENTNETPROFIT, data_KCFJCXSYJLR, how='left',
                                                left_on=['SECURITYCODE', 'REPORTDATE'],
                                                right_on=['SECURITYCODE', 'REPORTDATE'])
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR = data_PARENTNETPROFIT_KCFJCXSYJLR[
        (data_PARENTNETPROFIT_KCFJCXSYJLR['SECURITYCODE'] == x)]
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR = data_600529_PARENTNETPROFIT_KCFJCXSYJLR.drop(
        data_600529_PARENTNETPROFIT_KCFJCXSYJLR[
            data_600529_PARENTNETPROFIT_KCFJCXSYJLR['REPORTTIMETYPE'] == '7-9月季报'].index)
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR['REPORTDATE'] = pd.to_datetime(
        data_600529_PARENTNETPROFIT_KCFJCXSYJLR['REPORTDATE'], format='%Y-%m-%d')
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['REPORTDATE'].map(
        lambda x: x.year)
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['REPORTDATE'].map(
        lambda x: x.month)
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStableChangeRage'] = data_600529_PARENTNETPROFIT_KCFJCXSYJLR[
        'KCFJCXSYJLR']
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStable'] = data_600529_PARENTNETPROFIT_KCFJCXSYJLR[
        'KCFJCXSYJLR']
    data_600529_PARENTNETPROFIT_KCFJCXSYJLR = data_600529_PARENTNETPROFIT_KCFJCXSYJLR.drop(['NOTICEDATE'],
                                                                                           axis=1).drop_duplicates(
        keep='first').reset_index().drop(['index'], axis=1)

    # 历史盈利是否稳定

    for i in range(data_600529_PARENTNETPROFIT_KCFJCXSYJLR.shape[0]):
        try:
            year = data_600529_NETPROFIT_PARENTNETPROFIT['Year'][i]
            month = data_600529_NETPROFIT_PARENTNETPROFIT['Month'][i]
            #DP_1 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][i - 4]
            DP_1 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-1)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == month)].tolist()[0]
            #DP_2 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][i - 8]
            DP_2 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-2)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == month)].tolist()[0]

            #DP_3 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][i - 12]
            DP_3 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-3)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == month)].tolist()[0]

            DP_1_4 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-1)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == 12)].tolist()[0]
            DP_2_4 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-2)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == 12)].tolist()[0]
            DP_3_4 = data_600529_PARENTNETPROFIT_KCFJCXSYJLR['KCFJCXSYJLR'][(data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Year'] ==(year-3)) & (data_600529_PARENTNETPROFIT_KCFJCXSYJLR['Month'] == 12)].tolist()[0]
            PCT1 = DP_1 / DP_1_4
            PCT2 = DP_2 / DP_2_4
            PCT3 = DP_3 / DP_3_4
            MAD = max(abs(PCT1 - (PCT1 + PCT2 + PCT3) / 3), abs(PCT2 - (PCT1 + PCT2 + PCT3) / 3),
                      abs(PCT3 - (PCT1 + PCT2 + PCT3) / 3))

            data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStableChangeRage'][i] = MAD
            if MAD < 0.1:
                data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStable'][i] = True
            else:
                data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStable'][i] = False
        except:
            data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStableChangeRage'][i] = '未知'
            data_600529_PARENTNETPROFIT_KCFJCXSYJLR['IfprofitdisStable'][i] = '未知'

    # 剩余年度的净利润是否正常
    data_600529_NETPROFIT_PARENTNETPROFIT['historyprofitextroplaterate'] = data_600529_NETPROFIT_PARENTNETPROFIT[
        'NETPROFIT']
    data_600529_NETPROFIT_PARENTNETPROFIT['ifhistoryprofitextroplate'] = data_600529_NETPROFIT_PARENTNETPROFIT[
        'NETPROFIT']

    for i in range(data_600529_NETPROFIT_PARENTNETPROFIT.shape[0]):
        try:
            year = data_600529_NETPROFIT_PARENTNETPROFIT['Year'][i]
            month = data_600529_NETPROFIT_PARENTNETPROFIT['Month'][i]
            RP_T_1 = data_600529_NETPROFIT_PARENTNETPROFIT[
                (data_600529_NETPROFIT_PARENTNETPROFIT['Year'] == (year - 1)) & (
                            data_600529_NETPROFIT_PARENTNETPROFIT['Month'] == month)]['NETPROFIT'].tolist()[0]
            RP_T_2 = data_600529_NETPROFIT_PARENTNETPROFIT[
                (data_600529_NETPROFIT_PARENTNETPROFIT['Year'] == (year - 2)) & (
                            data_600529_NETPROFIT_PARENTNETPROFIT['Month'] == month)]['NETPROFIT'].tolist()[0]
            RP_T_3 = data_600529_NETPROFIT_PARENTNETPROFIT[
                (data_600529_NETPROFIT_PARENTNETPROFIT['Year'] == (year - 3)) & (
                            data_600529_NETPROFIT_PARENTNETPROFIT['Month'] == month)]['NETPROFIT'].tolist()[0]
            AVGRPn = (RP_T_2 + RP_T_3) / 2
            value = abs(RP_T_1 - AVGRPn) / AVGRPn
            data_600529_NETPROFIT_PARENTNETPROFIT['historyprofitextroplaterate'][i] = value

            if value < 2:
                data_600529_NETPROFIT_PARENTNETPROFIT['ifhistoryprofitextroplate'][i] = True
            else:
                data_600529_NETPROFIT_PARENTNETPROFIT['ifhistoryprofitextroplate'][i] = False

        except:
            data_600529_NETPROFIT_PARENTNETPROFIT['ifhistoryprofitextroplate'][i] = '未知'
            data_600529_NETPROFIT_PARENTNETPROFIT['historyprofitextroplaterate'][i] = '未知'

    total = pd.merge(data_600529_NETPROFIT_PARENTNETPROFIT, data_600529_PARENTNETPROFIT_KCFJCXSYJLR, how='left',
                     on=['Year', 'Month'])

    total = total[['SECURITYCODE_x', 'NETPROFIT_x', 'PARENTNETPROFIT_x', 'REPORTDATE_x',
                   'REPORTTIMETYPE_x', 'KCFJCXSYJLR', 'Year', 'Month', 'managesituationChangeRate',
                   'managesituationGreatChange', 'historyprofitextroplaterate',
                   'ifhistoryprofitextroplate', 'IfprofitdisStableChangeRage',
                   'IfprofitdisStable']]
    return total


def time_judge(x):
    if x in [3,4,5]:
        result=3
    elif x in [6,7,8]:
        result=6
    elif x in [9,10,11]:
        result=9
    elif x in [12,1,2]:
        result=12
    return result